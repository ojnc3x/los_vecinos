#= require jquery
#= require jquery_ujs
#= require foundation
#= require turbolinks
#= require foundation.min
#= require jquery.slick
#= require jquery.validate
#= require socialShare
#= require jquery.remotipart
# require_tree .

$ ->

  $(document).foundation()

  $("form#subscribe-now").validate
    rules:
      phone:
        required: true
    errorPlacement: (error, element) ->
      error.insertAfter element.parents('.input-group')
    messages:
      phone:
        required: true
    errorPlacement: (error, element) ->
      error.insertAfter element.parents('.input-group')
    messages:
      phone:
        required: "Ingrese un número válido"


  $('.share-chapter').socialShare
    social: 'facebook,twitter'
    title: "Serie web Los Vecinos, te la recomiendo!"
    shareUrl: "http://losvecinosserie.com"
    description: "¡La serie web favorita de los colombianos!"



  $(document).on 'click', 'a.clickable[href^="#"]', (e) ->
    # target element id
    id = $(this).attr('href')
    # target element
    $id = $(id)

    if $id.length == 0
      return
    # prevent standard hash navigation (avoid blinking in IE)
    e.preventDefault()
    e.stopPropagation()
    # top position relative to the document
    pos = $id.offset().top
    # animated top scrolling
    $('body, html').animate scrollTop: pos
    return


  $('.cast-wrapper').slick
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 4,
    autoplay: false,
    responsive: [
      {
        breakpoint: 801
        settings:
          slidesToShow: 2
          slidesToScroll: 2
      }
      {
        breakpoint: 481
        settings:
          slidesToShow: 1
          slidesToScroll: 1
      }
    ]
