class Episode < ApplicationRecord
  extend FriendlyId

  enum kind: [:teaser, :chapter]

  validates :title, :length, :content, :premiere_at, :url, :kind, presence: true

  validates :number, presence: true, if: Proc.new{|o| o.chapter?}


  URL_FORMATS = {
      regular: /^(https?:\/\/)?(www\.)?youtube.com\/watch\?(.*\&)?v=(?<id>[^&]+)/,
      shortened: /^(https?:\/\/)?(www\.)?youtu.be\/(?<id>[^&]+)/,
      embed: /^(https?:\/\/)?(www\.)?youtube.com\/embed\/(?<id>[^&]+)/,
      embed_as3: /^(https?:\/\/)?(www\.)?youtube.com\/v\/(?<id>[^?]+)/,
      chromeless_as3: /^(https?:\/\/)?(www\.)?youtube.com\/apiplayer\?video_id=(?<id>[^&]+)/
  }

  INVALID_CHARS = /[^a-zA-Z0-9\:\/\?\=\&\$\-\_\.\+\!\*\'\(\)\,]/


  friendly_id :title, use: [:slugged, :finders]

  has_attached_file :picture
  validates_attachment_content_type :picture, content_type: %r{\Aimage\/.*\Z}

  def picture_url
    self.picture.url
  end

  def has_invalid_chars?(youtube_url)
    !INVALID_CHARS.match(youtube_url).nil?
  end

  def extract_video_id(youtube_url)
    return nil if has_invalid_chars?(youtube_url)

    URL_FORMATS.values.each do |format_regex|
      match = format_regex.match(youtube_url)
      return match[:id] if match
    end
  end

  def youtube_url_id
    extract_video_id(self.url)
  end


end
