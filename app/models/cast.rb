class Cast < ApplicationRecord
  has_attached_file :picture
  validates_attachment_content_type :picture, content_type: %r{\Aimage\/.*\Z}

  def picture_url
    self.picture.url
  end

end
