class WebShow < ApplicationRecord
  has_attached_file :poster
  validates_attachment_content_type :poster, content_type: %r{\Aimage\/.*\Z}

end
