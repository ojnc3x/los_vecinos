class HomeController < ApplicationController
  layout "application"
  respond_to :js, :only => :subscribe, :layout => 'false'

  def index
    @web_show = WebShow.first rescue nil
    @episodes = Episode.where(kind: 1).reorder(created_at: :desc) rescue nil
    @last_episode = Episode.last rescue nil
    @cast = Cast.all rescue nil
  end

  def subscribe
    @subscriber = Subscriber.new
    @subscriber.phone = params[:phone]
    @success = @subscriber.save
    respond_to do |format|
      format.js
    end
  end

end
