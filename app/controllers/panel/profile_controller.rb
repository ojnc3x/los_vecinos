class Panel::ProfileController < Panel::ApplicationController

  def model
    User
  end

  def edit
    @user = model.find current_user.id
    redirect_to user_root_url if @user != current_user
  end

  def update
    @user = model.find current_user.id
    if @user.update_attributes item_params
      sign_in @user, :bypass => true
      redirect_to(edit_url, flash: { saved: true })
    else
      render template: 'panel/profile/edit'
    end

  end

  def item_params
    params.require(:user).permit(
      :name,
      :password,
    )
  end
end
