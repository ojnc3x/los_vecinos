class Panel::CastsController < Panel::ApplicationController
  protect_from_forgery with: :exception, unless: -> { request.format.json? }

  def model
    Cast
  end

  def index
    @items = model.where(conditions)
    add_breadcrumb model.model_name.human(count: :many), index_url

    respond_to do |format|
      format.html { render template: 'panel/casts/index' }
      format.json { render json: @items }
    end
  end

  def item_params
    params.require(:cast).permit!
  end

end
