# frozen_string_literal: true
module Panel
  #
  class UsersController < PanelController
    include Tabled


    def model
      User
    end

    def index
      @items = model.where(conditions)
      add_breadcrumb model.model_name.human(count: :many), index_url

      respond_to do |format|
        format.html { render template: 'panel/users/index' }
        format.json { render json: @items }
      end
    end

    def item_params
      params.require(:user).permit!
    end


  end
end
