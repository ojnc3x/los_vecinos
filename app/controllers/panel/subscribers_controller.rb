class Panel::SubscribersController < Panel::ApplicationController
  protect_from_forgery with: :exception, unless: -> { request.format.json? }

  def model
    Subscriber
  end

  def index
    @items = model.where(conditions)
    add_breadcrumb model.model_name.human(count: :many), index_url

    respond_to do |format|
      format.html { render template: 'panel/subscribers/index' }
      format.json { render json: @items }
      format.csv { send_data @items.to_csv, filename: "Suscriptores-#{Date.today}.csv" }
    end
  end

end
