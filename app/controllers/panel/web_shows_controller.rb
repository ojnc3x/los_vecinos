class Panel::WebShowsController < Panel::ApplicationController
  protect_from_forgery with: :exception, unless: -> { request.format.json? }

  def model
    WebShow
  end

  def index
    @items = model.where(conditions)
    add_breadcrumb model.model_name.human(count: :many), index_url

    respond_to do |format|
      format.html { render template: 'panel/web_shows/index' }
      format.json { render json: @items }
    end
  end

  def item_params
    params.require(:web_show).permit!
  end


end
