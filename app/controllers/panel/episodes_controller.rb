class Panel::EpisodesController < Panel::ApplicationController
  protect_from_forgery with: :exception, unless: -> { request.format.json? }

  def model
    Episode
  end

  def index
    @items = model.where(conditions).uniq
    add_breadcrumb model.model_name.human(count: :many), index_url

    respond_to do |format|
      format.html { render template: 'panel/episodes/index' }
      format.json { render json: @items }
    end
  end

  def item_params
    params.require(:episode).permit!
  end

end
