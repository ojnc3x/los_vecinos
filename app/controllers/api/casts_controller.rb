module Api
  class CastsController < ApplicationController
    include ActionController::Serialization
    require 'json'

    def index
      render json: Cast.all
    end

  end
end
