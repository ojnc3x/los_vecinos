module Api
  class SubscribersController < ApplicationController
    skip_before_action :verify_authenticity_token, :only => :create
    include ActionController::Serialization
    require 'json'

    respond_to :json

    def create
      @subscriber = Subscriber.new(item_params)
      if @subscriber.save
        render json: {
          status: 201,
          message: "Te has suscrito exitosamente."
        }.to_json
      else
        render json: {
          status: 422,
          message: "Ha ocurrido un error al procesar los datos."
        }.to_json
      end
    end

    def item_params
      params.require(:contact_message).permit(
        :phone,
      )
    end

  end
end
