module Api
  class EpisodesController < ApplicationController
    include ActionController::Serialization

    def index
      render json: Episode.all
    end

    def show
      render json: Episode.find(params[:id])
    end

    def last_episode
      render json: Episode.reorder("created_at DESC").limit(1)
    end

  end
end
