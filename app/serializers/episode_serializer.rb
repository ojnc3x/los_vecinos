class EpisodeSerializer < ActiveModel::Serializer
  attributes :id, :title, :description, :picture_url, :length, :content,
              :premiere_at, :youtube_url_id
end
