class CastSerializer < ActiveModel::Serializer
  attributes :id, :name, :description, :picture_url
end
