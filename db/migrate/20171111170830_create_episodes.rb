class CreateEpisodes < ActiveRecord::Migration[5.0]
  def change
    create_table :episodes do |t|
      t.string :title
      t.string :description
      t.string :length
      t.text :content
      t.datetime :premiere_at
      t.string :url
      t.attachment :picture
      t.timestamps
    end
  end
end
