class CreateWebShows < ActiveRecord::Migration[5.0]
  def change
    create_table :web_shows do |t|
      t.string :title
      t.text :synopsis
      t.date :world_premiere
      t.attachment :poster
      t.timestamps
    end
  end
end
