Rails.application.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'
  devise_for :users, controllers: { sessions: 'users/sessions' }
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: 'home#index'

  resources :home, only: [:index] do
    collection do
      post 'subscribe'
    end
  end

  resources :episodes, only: [:show]
  resources :casts, only: [:show]

  namespace :panel do
    root to: 'dashboard#index'

    resources :profile, only: [:edit, :update]

    resources :episodes
    resources :users
    resources :web_shows
    resources :casts
    resources :subscribers, only: [:index, :show] do
      collection do
        get :export
      end
    end
  end

  namespace :api do
    resources :episodes, only: [:index, :show] do
      collection do
        get 'last_episode'
      end
    end
    resources :subscribers, only: [:create]
    resources :casts, only: [:index]
  end

end
